package com.grupodos.geoffers.sychronization;

import com.grupodos.geoffers.replication.ReplicationClientCompanyOffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Component;

@Component
public class SychronizationReplicaManager {
	private static final Logger log = LoggerFactory.getLogger(SychronizationReplicaManager.class);
	private final ReplicationClientCompanyOffer replicationClientCompanyOffer;

	public SychronizationReplicaManager(ReplicationClientCompanyOffer replicationClientCompanyOffer) {
		this.replicationClientCompanyOffer = replicationClientCompanyOffer;
	}

	public void performReplicationDatabase() {
		log.info("Init synchronization process");
		replicationClientCompanyOffer.syncAllDatabase();
		log.info("End synchronization process");
	}
}
