package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.Cliente;
import com.grupodos.geoffers.modelos.ClienteUpgrade;
import com.grupodos.geoffers.modelos.Nivel;
import com.grupodos.geoffers.modelos.Oferta;
import com.grupodos.geoffers.servicios.repositorio.ClienteOfertaRepository;
import com.grupodos.geoffers.servicios.repositorio.ClienteRepository;
import com.grupodos.geoffers.servicios.repositorio.NivelRepository;
import com.grupodos.geoffers.servicios.repositorio.OfertaRepository;
import com.grupodos.geoffers.servicios.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    OfertaRepository ofertaRepository;

    @Autowired
    ClienteOfertaRepository clienteOfertaRepository;

    @Autowired
    NivelRepository nivelRepository;

    @Override
    public List<Cliente> obtenerClientes() {
        return clienteRepository.findAll();
    }

    @Override
    public Cliente obtenerClienteByDocumento(String documento) {
        return clienteRepository.findByDocumento(documento);
    }

    @Override
    public ClienteUpgrade obtenerClienteUpgradeByDocumento(String documento) {
        Cliente cliente = clienteRepository.findByDocumento(documento);
        if (cliente != null) {
            Long ofertasRegistradas = clienteOfertaRepository.findAll().stream().filter(x -> x.getCliente().getDocumento().equals(documento)).count();
            Nivel siguienteNivel = nivelRepository.findByEstrellas(cliente.getEstrellas()+1);
            List<Oferta> ofertas = ofertaRepository.findAll().stream().map(x -> {
                    if (x.getNivel() == null) {
                        Nivel nivel = new Nivel();
                        nivel.setEstrellas(1);
                        x.setNivel(nivel);
                    }
                    return x;
                }).filter(x -> x.getNivel().getEstrellas().equals(cliente.getEstrellas()+1)).collect(Collectors.toList());
            ClienteUpgrade clienteUpgrade = new ClienteUpgrade();
            clienteUpgrade.setDocumento(cliente.getDocumento());
            clienteUpgrade.setNombres(cliente.getNombres());
            clienteUpgrade.setApellidos(cliente.getApellidos());
            clienteUpgrade.setEstrellas(cliente.getEstrellas());
            clienteUpgrade.setOfertasRegistradas(ofertasRegistradas.intValue());
            clienteUpgrade.setSiguienteNivel(siguienteNivel);
            clienteUpgrade.setOfertasSiguienteNivel(ofertas.size());
            return clienteUpgrade;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado.");
        }
    }

    @Override
    public void agregarCliente(Cliente cliente) {
        clienteRepository.insert(cliente);
    }

    @Override
    public void upgradeCliente(Cliente cliente) {
        Cliente clienteActual = clienteRepository.findByDocumento(cliente.getDocumento());
        clienteActual.setEstrellas(cliente.getEstrellas());
        clienteActual.setFechaUpgrade(new Date());
        clienteRepository.save(clienteActual);
    }
}
