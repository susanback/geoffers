package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.CompanyOffer;
import com.grupodos.geoffers.servicios.ServicioOfferCompany;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompanyOffer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioOfferCompanyOfferImpl  implements ServicioOfferCompany {

    @Autowired
    RepositorioCompanyOffer repositorioCompanyOffer;



    @Override
    public void agregarOfertaCompania(String ruc, CompanyOffer companyOffer) {
        companyOffer.setRucCompany(ruc);
        repositorioCompanyOffer.insert(companyOffer);
    }

    @Override
    public List<CompanyOffer> obtenerOfertasCompany() {
        return this.repositorioCompanyOffer.findAll();
    }

    @Override
    public CompanyOffer obtenerOfertasCompany(String id) {
        final var quizasOfertasCompany = this.repositorioCompanyOffer.findById(id);
        if (!quizasOfertasCompany.isPresent())
            throw new RuntimeException("No existe la id empresa oferta " + id);
        return quizasOfertasCompany.get();
    }

    @Override
    public void borrarOfertasCompany(String id) {
        this.repositorioCompanyOffer.deleteById(id);
    }

    @Override
    public List<CompanyOffer> obtenerCompanynroRUC(String nroRUC) {
        return this.repositorioCompanyOffer.findByRucCompany(nroRUC);
    }


    @Override
    public void emparcharOfertaCompany(String ruc, CompanyOffer companyOffer) {
        companyOffer.setRucCompany(ruc);
        if(!this.repositorioCompanyOffer.existsById(companyOffer.getRucCompany()))
            throw new RuntimeException("No existe la cuenta número" + companyOffer.getRucCompany());

    }
}