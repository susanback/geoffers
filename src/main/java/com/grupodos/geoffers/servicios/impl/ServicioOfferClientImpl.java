package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.OfferClient;
import com.grupodos.geoffers.replication.ReplicationClientCompanyOffer;
import com.grupodos.geoffers.servicios.ServicioOfferClient;
import com.grupodos.geoffers.servicios.repositorio.RepositorioOfferClient;

import org.springframework.stereotype.Service;

@Service
public class ServicioOfferClientImpl implements ServicioOfferClient {

	private final RepositorioOfferClient repositorioOfferClient;
	private final ReplicationClientCompanyOffer replicationClientCompanyOffer;

	public ServicioOfferClientImpl(RepositorioOfferClient repositorioOfferClient, ReplicationClientCompanyOffer replicationClientCompanyOffer) {
		this.repositorioOfferClient = repositorioOfferClient;
		this.replicationClientCompanyOffer = replicationClientCompanyOffer;
	}

	@Override
	public void agregarOfertaClienteCompania(OfferClient offerClient) {
		replicationClientCompanyOffer.synchOfferClient(offerClient);
		repositorioOfferClient.insert(offerClient);
	}
}
