package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.dto.CompanyDto;
import com.grupodos.geoffers.modelos.CompanyModel;
import com.grupodos.geoffers.servicios.ServicioCompany;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompany;

import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

@Service
public class ServicioCompanyImpl implements ServicioCompany {

	private final RepositorioCompany repositorioLocal;

	public ServicioCompanyImpl(RepositorioCompany repositorioLocal) {
		this.repositorioLocal = repositorioLocal;
	}

	private CompanyModel map(CompanyDto dto) {
		CompanyModel model = new CompanyModel();
		model.setRuc(dto.getRuc());
		model.setRazonSocial(dto.getRazonSocial());
		model.setNombreComercial(dto.getNombreComercial());
		model.setPaginaWeb(dto.getPaginaWeb());
		model.setDireccion(dto.getDireccion());
		model.setSede(dto.getSede());
		GeoJsonPoint location = new GeoJsonPoint(dto.getLongitud(), dto.getLatitud());
		model.setLocation(location);
		return model;
	}


	@Override
	public void crearCompany(CompanyDto dto) {
		CompanyModel model = this.map(dto);
		this.repositorioLocal.insert(model);
	}

}
