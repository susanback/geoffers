package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.Cliente;
import com.grupodos.geoffers.modelos.Nivel;
import com.grupodos.geoffers.modelos.Oferta;
import com.grupodos.geoffers.servicios.repositorio.ClienteRepository;
import com.grupodos.geoffers.servicios.repositorio.OfertaRepository;
import com.grupodos.geoffers.servicios.OfertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfertaServiceImpl implements OfertaService {

    @Autowired
    OfertaRepository ofertaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public List<Oferta> obtenerOfertas() {
        return ofertaRepository.findAll().stream().map(x -> {
            if (x.getNivel() == null) {
                Nivel nivel = new Nivel();
                nivel.setEstrellas(1);
                x.setNivel(nivel);
            }
            return x;
        }).collect(Collectors.toList());
    }

    @Override
    public Oferta obtenerOfertaById(String id) {
        Optional<Oferta> oferta = ofertaRepository.findById(id);
        if (oferta.isPresent()) {
            return oferta.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Oferta no encontrada.");
        }
    }

    @Override
    public List<Oferta> obtenerOfertasxNivel(Integer estrellas) {
        List<Oferta> ofertas = ofertaRepository.findAll().stream().map(x -> {
            if (x.getNivel() == null) {
                Nivel nivel = new Nivel();
                nivel.setEstrellas(1);
                x.setNivel(nivel);
            }
            return x;
        }).collect(Collectors.toList());
        return ofertas.stream().filter(x -> x.getNivel().getEstrellas().equals(estrellas)).collect(Collectors.toList());
    }

    @Override
    public List<Oferta> obtenerOfertasxCliente(String documento) {
        Cliente cliente = clienteRepository.findByDocumento(documento);
        if (cliente != null) {
            List<Oferta> ofertas = ofertaRepository.findAll().stream().map(x -> {
                if (x.getNivel() == null) {
                    Nivel nivel = new Nivel();
                    nivel.setEstrellas(1);
                    x.setNivel(nivel);
                }
                return x;
            }).collect(Collectors.toList());
            return ofertas.stream().filter(x -> x.getNivel().getEstrellas() <= cliente.getEstrellas()).collect(Collectors.toList());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado.");
        }
    }

    @Override
    public void agregarOferta(Oferta oferta) {
        ofertaRepository.insert(oferta);
    }

    @Override
    public void actualizarNivel(Oferta oferta) {
        Optional<Oferta> ofertaActual = ofertaRepository.findById(oferta.getId());
        if (ofertaActual.isPresent()) {
            Oferta ofertaActualizada = ofertaActual.get();
            ofertaActualizada.setNivel(oferta.getNivel());
            ofertaRepository.save(ofertaActualizada);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Oferta no encontrada.");
        }
    }
}
