package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.Nivel;
import com.grupodos.geoffers.servicios.repositorio.NivelRepository;
import com.grupodos.geoffers.servicios.NivelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class NivelServiceImpl implements NivelService {

    @Autowired
    NivelRepository nivelRepository;

    @Override
    public List<Nivel> obtenerNiveles() {
        return nivelRepository.findAll();
    }

    @Override
    public Nivel obtenerNivelByEstrellas(Integer estrellas) {
        Nivel nivel = nivelRepository.findByEstrellas(estrellas);
        if (nivel != null) {
            return nivel;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Nivel no encontrado.");
        }
    }

    @Override
    public void agregarNivel(Nivel nivel) {
        nivelRepository.insert(nivel);
    }
}
