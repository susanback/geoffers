package com.grupodos.geoffers.servicios.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.grupodos.geoffers.dto.GeOfferRequest;
import com.grupodos.geoffers.dto.GeOfferResponse;
import com.grupodos.geoffers.modelos.CompanyModel;
import com.grupodos.geoffers.replication.modelos.ClientCompanyOffer;
import com.grupodos.geoffers.servicios.ServicioGeOffer;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompany;
import com.grupodos.geoffers.servicios.repositorio.RepositoryClientCompanyOffer;

import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

@Service
public class ServicioGeOfferImpl implements ServicioGeOffer {

	private final RepositorioCompany repositorioCompany;
	private final RepositoryClientCompanyOffer repositoryClientCompanyOffer;

	public ServicioGeOfferImpl(RepositorioCompany repositorioCompany, RepositoryClientCompanyOffer repositoryClientCompanyOffer) {
		this.repositorioCompany = repositorioCompany;
		this.repositoryClientCompanyOffer = repositoryClientCompanyOffer;
	}

	private List<CompanyModel> getCompanyNear(GeOfferRequest request) {
		Point point = new Point(request.getLongitud(), request.getLatitud());
		return repositorioCompany.getCompaniesNear(point)//
				.getContent()//
				.stream()//
				.map(GeoResult::getContent)//
				.collect(Collectors.toList())//
				;
	}

	@Override
	public List<GeOfferResponse> getGeOffers(GeOfferRequest request) {
		List<CompanyModel> lst = this.getCompanyNear(request);
		if (lst.isEmpty())
			return Collections.emptyList();

		List<String> rucs = lst.stream()//
				.map(CompanyModel::getRuc)//
				.collect(Collectors.toList())//
				;

		List<ClientCompanyOffer> results = repositoryClientCompanyOffer
				.findAllByCoClientEqualsAndCoCompanyIn(request.getCliente(), rucs);

		List<GeOfferResponse> lstResponse = new ArrayList<>();
		for (ClientCompanyOffer offer : results) {
			GeOfferResponse rs = new GeOfferResponse();
			rs.setDescripcion(offer.getDescripcionOffer());
			rs.setEmpresa(offer.getNombreComercialCompany());
			rs.setImagenUrl(offer.getImagenOffer());
			rs.setLatitud(offer.getLatitudCompany());
			rs.setLongitud(offer.getLongitudCompany());
			rs.setProducto(offer.getProductoOffer());
			lstResponse.add(rs);
		}
		return lstResponse;
	}
}
