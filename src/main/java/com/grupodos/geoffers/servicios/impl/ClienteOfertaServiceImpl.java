package com.grupodos.geoffers.servicios.impl;

import com.grupodos.geoffers.modelos.Cliente;
import com.grupodos.geoffers.modelos.ClienteOferta;
import com.grupodos.geoffers.servicios.repositorio.ClienteOfertaRepository;
import com.grupodos.geoffers.servicios.ClienteOfertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClienteOfertaServiceImpl implements ClienteOfertaService {

    @Autowired
    ClienteOfertaRepository clienteOfertaRepository;

    @Override
    public List<ClienteOferta> obtenerClienteOfertas() {
        return clienteOfertaRepository.findAll();
    }

    @Override
    public List<ClienteOferta> obtenerClienteOfertasByDocumento(String documento) {
        List<ClienteOferta> clienteOfertas = clienteOfertaRepository.findAll();
        return clienteOfertas.stream().filter(x -> x.getCliente().getDocumento().equals(documento)).collect(Collectors.toList());
    }

    @Override
    public void agregarClienteOferta(String documento, ClienteOferta clienteOferta) {
        Cliente cliente = new Cliente();
        cliente.setDocumento(documento);
        clienteOferta.setCliente(cliente);
        clienteOfertaRepository.insert(clienteOferta);
    }
}
