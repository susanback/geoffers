package com.grupodos.geoffers.servicios.repositorio.impl;

import com.grupodos.geoffers.modelos.CompanyModel;
import com.grupodos.geoffers.servicios.repositorio.RepositorioGeOffer;

import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class RepositorioGeOfferImpl implements RepositorioGeOffer {
	private final MongoTemplate mongoTemplate;
	private final double MAX_DISTANCE = .45;

	public RepositorioGeOfferImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public GeoResults<CompanyModel> getCompaniesNear(Point point) {
		Query query = new Query();
		NearQuery n = NearQuery.near(point);
		n.spherical(true);
		n.inKilometers();
		n.maxDistance(MAX_DISTANCE);
		n.query(query);
		return mongoTemplate.geoNear(n, CompanyModel.class);
	}
}
