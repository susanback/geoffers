package com.grupodos.geoffers.servicios.repositorio;

import java.util.List;

import com.grupodos.geoffers.replication.modelos.ClientCompanyOffer;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositoryClientCompanyOffer extends MongoRepository<ClientCompanyOffer, String> {
	List<ClientCompanyOffer> findAllByCoClientEqualsAndCoCompanyIn(String coCliente, List<String> companies);
}
