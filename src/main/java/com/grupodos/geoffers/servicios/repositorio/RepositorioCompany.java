package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.CompanyModel;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCompany extends MongoRepository<CompanyModel, String>, RepositorioGeOffer {
}