package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.ClienteOferta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClienteOfertaRepository extends MongoRepository<ClienteOferta, String> {
}
