package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.OfferClient;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioOfferClient extends MongoRepository<OfferClient, String> {

}
