package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.Oferta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OfertaRepository extends MongoRepository<Oferta, String> {

}
