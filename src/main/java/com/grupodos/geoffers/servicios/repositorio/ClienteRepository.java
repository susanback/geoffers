package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClienteRepository extends MongoRepository<Cliente, String> {

    Cliente findByDocumento(String documento);

}
