package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.CompanyModel;

import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Point;

public interface RepositorioGeOffer {
	GeoResults<CompanyModel> getCompaniesNear(Point point);
}
