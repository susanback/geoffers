package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.Nivel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NivelRepository extends MongoRepository<Nivel, String> {

    Nivel findByEstrellas(Integer estrellas);

}
