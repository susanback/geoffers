package com.grupodos.geoffers.servicios.repositorio;

import com.grupodos.geoffers.modelos.CompanyOffer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface RepositorioCompanyOffer extends MongoRepository<CompanyOffer, String> {

    public List<CompanyOffer> findByRucCompany(String rucCompany);

    public void deleteById(String id);
}
