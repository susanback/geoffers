package com.grupodos.geoffers.servicios;

import com.grupodos.geoffers.modelos.Nivel;

import java.util.List;

public interface NivelService {

    List<Nivel> obtenerNiveles();

    Nivel obtenerNivelByEstrellas(Integer estrellas);

    void agregarNivel(Nivel nivel);
}
