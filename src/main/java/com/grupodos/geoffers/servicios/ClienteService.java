package com.grupodos.geoffers.servicios;

import com.grupodos.geoffers.modelos.Cliente;
import com.grupodos.geoffers.modelos.ClienteUpgrade;

import java.util.List;

public interface ClienteService {

    List<Cliente> obtenerClientes();

    Cliente obtenerClienteByDocumento(String documento);

    ClienteUpgrade obtenerClienteUpgradeByDocumento(String documento);

    void agregarCliente(Cliente cliente);

    void upgradeCliente(Cliente cliente);
}
