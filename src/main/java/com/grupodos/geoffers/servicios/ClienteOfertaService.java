package com.grupodos.geoffers.servicios;

import com.grupodos.geoffers.modelos.ClienteOferta;

import java.util.List;

public interface ClienteOfertaService {

    List<ClienteOferta> obtenerClienteOfertas();

    List<ClienteOferta> obtenerClienteOfertasByDocumento(String documento);

    void agregarClienteOferta(String documento, ClienteOferta clienteOferta);
}
