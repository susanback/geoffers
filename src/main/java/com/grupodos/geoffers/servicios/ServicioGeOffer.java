package com.grupodos.geoffers.servicios;

import java.util.List;

import com.grupodos.geoffers.dto.GeOfferRequest;
import com.grupodos.geoffers.dto.GeOfferResponse;

public interface ServicioGeOffer {

	List<GeOfferResponse> getGeOffers(GeOfferRequest request);
}
