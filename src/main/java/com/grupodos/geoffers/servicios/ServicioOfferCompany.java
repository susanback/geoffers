package com.grupodos.geoffers.servicios;

import com.grupodos.geoffers.modelos.CompanyOffer;

import java.util.List;


public interface ServicioOfferCompany {

    void agregarOfertaCompania(String ruc, CompanyOffer companyOffer);
    public List<CompanyOffer> obtenerOfertasCompany();

    public CompanyOffer obtenerOfertasCompany(String id);

    public List<CompanyOffer> obtenerCompanynroRUC(String nroRUC);

    void emparcharOfertaCompany(String ruc, CompanyOffer companyOffer);

    public void borrarOfertasCompany(String id);

}