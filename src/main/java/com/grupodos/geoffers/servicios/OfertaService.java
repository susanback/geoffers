package com.grupodos.geoffers.servicios;

import com.grupodos.geoffers.modelos.Oferta;

import java.util.List;

public interface OfertaService {

    List<Oferta> obtenerOfertas();

    Oferta obtenerOfertaById(String id);

    List<Oferta> obtenerOfertasxNivel(Integer estrellas);

    List<Oferta> obtenerOfertasxCliente(String documento);

    void agregarOferta(Oferta oferta);

    void actualizarNivel(Oferta oferta);
}
