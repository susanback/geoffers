package com.grupodos.geoffers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoffersApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeoffersApplication.class, args);
	}

}
