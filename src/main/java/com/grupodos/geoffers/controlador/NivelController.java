package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.modelos.Nivel;
import com.grupodos.geoffers.servicios.NivelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.LEVELS)
public class NivelController {

    @Autowired
    NivelService nivelService;

    @GetMapping
    public CollectionModel obtenerNiveles() {
        List<Nivel> nivels = nivelService.obtenerNiveles();
        List<EntityModel> nivelsModel = nivels.stream().map(x -> {
            Link self = linkTo(methodOn(this.getClass()).obtenerNivel(x.getEstrellas())).withSelfRel();
            return EntityModel.of(x, self);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerNiveles()).withSelfRel();
        return CollectionModel.of(nivelsModel, self);
    }

    @GetMapping("/{estrellas}")
    public EntityModel obtenerNivel(@PathVariable Integer estrellas) {
        Nivel nivel = nivelService.obtenerNivelByEstrellas(estrellas);
        Link self = linkTo(methodOn(this.getClass()).obtenerNivel(estrellas)).withSelfRel();
        return EntityModel.of(nivel, self);
    }

    @PostMapping
    public ResponseEntity agregarNivel(@RequestBody Nivel nivel) {
        nivelService.agregarNivel(nivel);
        var metodoObtenerNivel = methodOn(this.getClass()).obtenerNivel(nivel.getEstrellas());
        URI location = linkTo(metodoObtenerNivel).toUri();
        return ResponseEntity.created(location).build();
    }
}
