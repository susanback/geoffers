package com.grupodos.geoffers.controlador;


import com.grupodos.geoffers.modelos.CompanyOffer;
import com.grupodos.geoffers.servicios.ServicioOfferCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(Rutas.OFFERS)
public class ControladorOffers {

    @Autowired
    ServicioOfferCompany servicioCompanyOffer;

    @GetMapping("CompanyOffer")
    public List<CompanyOffer> obtenerOfertasCompany(){
        return this.servicioCompanyOffer.obtenerOfertasCompany();
    }


}
