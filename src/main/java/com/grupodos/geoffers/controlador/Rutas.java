package com.grupodos.geoffers.controlador;

public class Rutas {
	public static final String BASE = "/api/v1";
	public static final String COMPANYS = BASE + "/companys";
	public static final String CLIENTS = BASE + "/clients";
	public static final String OFFERS = BASE + "/ofertas";
	public static final String PATH_GEOFFER = BASE + "/geoffer";
	public static final String LEVELS = BASE + "/levels";

}
