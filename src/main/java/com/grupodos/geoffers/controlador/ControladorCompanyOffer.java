package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.modelos.CompanyOffer;
import com.grupodos.geoffers.servicios.ServicioOfferCompany;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompanyOffer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.COMPANYS)
public class ControladorCompanyOffer {

    @Autowired
    ServicioOfferCompany servicioCompanyOffer;

    @PostMapping("/{ruc}/offer")
    public void agregarOfertasCompany(@PathVariable String ruc, @RequestBody CompanyOffer companyOffer) {
        servicioCompanyOffer.agregarOfertaCompania(ruc, companyOffer);
    }

    /*@GetMapping("CompanyOffer")
    public List<CompanyOffer> obtenerOfertasCompany(){
        return this.servicioCompanyOffer.obtenerOfertasCompany();
    }*/

    @GetMapping("/{nroRUC}/offers")
    public ResponseEntity<List<CompanyOffer>> obtenerCompanynroRUC(@PathVariable("nroRUC") String nroRuc){

        try{
            final var numerosRuc = this.servicioCompanyOffer.obtenerCompanynroRUC(nroRuc);
            return ResponseEntity.ok(numerosRuc);
        } catch (RuntimeException x) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{id}")
    public CompanyOffer obtenerunaOfertasCompany(@PathVariable String id) {
        try {
            return this.servicioCompanyOffer.obtenerOfertasCompany(id);
        } catch(RuntimeException x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnOfertasCompany(@PathVariable String id) {
        try {
            this.servicioCompanyOffer.borrarOfertasCompany(id);
        } catch(RuntimeException x) {}
    }

}

