package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.dto.CompanyDto;
import com.grupodos.geoffers.servicios.ServicioCompany;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Rutas.COMPANYS)
public class ControladorCompany {

	private final ServicioCompany servicioCompany;

	public ControladorCompany(ServicioCompany servicioCompany) {
		this.servicioCompany = servicioCompany;
	}

	@PostMapping
	public void crearCompany(@RequestBody CompanyDto company) {
		this.servicioCompany.crearCompany(company);
	}
}
