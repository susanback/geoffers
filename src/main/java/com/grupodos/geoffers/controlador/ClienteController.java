package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.modelos.Cliente;
import com.grupodos.geoffers.modelos.ClienteOferta;
import com.grupodos.geoffers.modelos.ClienteUpgrade;
import com.grupodos.geoffers.servicios.ClienteOfertaService;
import com.grupodos.geoffers.servicios.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTS)
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    ClienteOfertaService clienteOfertaService;

    @GetMapping
    public CollectionModel obtenerClientes() {
        List<Cliente> clientes = clienteService.obtenerClientes();
        List<EntityModel> clientesModel = clientes.stream().map(x -> {
            Link self = linkTo(methodOn(this.getClass()).obtenerCliente(x.getDocumento())).withSelfRel();
            return EntityModel.of(x, self);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerClientes()).withSelfRel();
        return CollectionModel.of(clientesModel, self);
    }

    @GetMapping("/{documento}")
    public EntityModel obtenerCliente(@PathVariable String documento) {
        Cliente cliente = clienteService.obtenerClienteByDocumento(documento);
        Link self = linkTo(methodOn(this.getClass()).obtenerCliente(documento)).withSelfRel();
        return EntityModel.of(cliente, self);
    }

    @GetMapping("/{documento}/upgrade")
    public EntityModel obtenerClienteUpgrade(@PathVariable String documento) {
        ClienteUpgrade cliente = clienteService.obtenerClienteUpgradeByDocumento(documento);
        Link self = linkTo(methodOn(this.getClass()).obtenerClienteUpgrade(documento)).withSelfRel();
        return EntityModel.of(cliente, self);
    }

    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {
        clienteService.agregarCliente(cliente);
        var metodoObtenerCliente = methodOn(this.getClass()).obtenerCliente(cliente.getDocumento());
        URI location = linkTo(metodoObtenerCliente).toUri();
        return ResponseEntity.created(location).build();
    }

    @PatchMapping("/{documento}/upgrade")
    public ResponseEntity upgradeCliente(@PathVariable String documento, @RequestBody Cliente cliente) {
        cliente.setDocumento(documento);
        clienteService.upgradeCliente(cliente);
        return new ResponseEntity<>("Upgrade cliente correctamente.", HttpStatus.OK);
    }

    @GetMapping("/{documento}/ofertas")
    public CollectionModel obtenerClienteOfertas(@PathVariable String documento) {
        List<ClienteOferta> ofertas = clienteOfertaService.obtenerClienteOfertasByDocumento(documento);
        List<EntityModel> clientesModel = ofertas.stream().map(x -> {
            Link self = linkTo(methodOn(OfertasController.class).obtenerOferta(x.getOferta().getId())).withSelfRel();
            return EntityModel.of(x, self);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerClientes()).withSelfRel();
        return CollectionModel.of(clientesModel, self);
    }

    @PostMapping("/{documento}/ofertas")
    public ResponseEntity agregarClienteOferta(@PathVariable String documento, @RequestBody ClienteOferta clienteOferta) {
        clienteOfertaService.agregarClienteOferta(documento, clienteOferta);
        return new ResponseEntity<>("Oferta asignada correctamente.", HttpStatus.CREATED);
    }
}
