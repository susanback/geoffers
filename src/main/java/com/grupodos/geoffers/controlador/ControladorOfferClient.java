package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.modelos.OfferClient;
import com.grupodos.geoffers.servicios.ServicioOfferClient;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Rutas.CLIENTS)
public class ControladorOfferClient {

	private final ServicioOfferClient servicioOfferClient;

	public ControladorOfferClient(ServicioOfferClient servicioOfferClient) {
		this.servicioOfferClient = servicioOfferClient;
	}

	@PostMapping("/{document}/offer")
	public void agregarOfertas(@PathVariable String document, @RequestBody OfferClient offerClient) {
		offerClient.setDniCliente(document);
		servicioOfferClient.agregarOfertaClienteCompania(offerClient);
	}
}
