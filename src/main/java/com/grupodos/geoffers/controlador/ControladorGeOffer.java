package com.grupodos.geoffers.controlador;

import java.util.List;

import com.grupodos.geoffers.dto.GeOfferRequest;
import com.grupodos.geoffers.dto.GeOfferResponse;
import com.grupodos.geoffers.servicios.ServicioGeOffer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.grupodos.geoffers.controlador.Rutas.PATH_GEOFFER;

@RestController
@RequestMapping(PATH_GEOFFER)
public class ControladorGeOffer {

	private final ServicioGeOffer servicioGeOffer;

	public ControladorGeOffer(ServicioGeOffer servicioGeOffer) {
		this.servicioGeOffer = servicioGeOffer;
	}

	@GetMapping
	public ResponseEntity<List<GeOfferResponse>> getGeOffers(@RequestParam("cliente") String cliente //
			, @RequestParam("latitud") Double latitud //
			, @RequestParam("longitud") Double longitud) {
		GeOfferRequest request = new GeOfferRequest();
		request.setCliente(cliente);
		request.setLatitud(latitud);
		request.setLongitud(longitud);
		List<GeOfferResponse> lst = servicioGeOffer.getGeOffers(request);
		if (lst.isEmpty())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(lst);
	}
}
