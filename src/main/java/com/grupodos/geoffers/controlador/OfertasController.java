package com.grupodos.geoffers.controlador;

import com.grupodos.geoffers.modelos.Oferta;
import com.grupodos.geoffers.servicios.OfertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.OFFERS)
public class OfertasController {

    @Autowired
    OfertaService ofertaService;

    @GetMapping
    public CollectionModel obtenerOfertas(@RequestParam(required = false) Integer nivel, @RequestParam(required = false) String documento) {
        List<Oferta> ofertas;
        if (nivel != null) {
            ofertas = ofertaService.obtenerOfertasxNivel(nivel);
        } else if (StringUtils.hasText(documento)) {
            ofertas = ofertaService.obtenerOfertasxCliente(documento);
        } else {
            ofertas = ofertaService.obtenerOfertas();
        }
        List<EntityModel> ofertasModel = ofertas.stream().map(x -> {
            Link self = linkTo(methodOn(this.getClass()).obtenerOferta(x.getId())).withSelfRel();
            return EntityModel.of(x, self);
        }).collect(Collectors.toList());
        Link self = linkTo(methodOn(this.getClass()).obtenerOfertas(nivel, documento)).withSelfRel();
        return CollectionModel.of(ofertasModel, self);
    }

    @GetMapping("/{id}")
    public EntityModel obtenerOferta(@PathVariable String id) {
        Oferta oferta = ofertaService.obtenerOfertaById(id);
        Link self = linkTo(methodOn(this.getClass()).obtenerOferta(id)).withSelfRel();
        return EntityModel.of(oferta, self);
    }

    @PostMapping
    public ResponseEntity agregarOferta(@RequestBody Oferta oferta) {
        ofertaService.agregarOferta(oferta);
        return new ResponseEntity<>("Oferta creada correctamente.", HttpStatus.CREATED);
    }

    @PatchMapping("/{id}/level")
    public ResponseEntity actualizarNivel(@PathVariable String id, @RequestBody Oferta oferta) {
        oferta.setId(id);
        ofertaService.actualizarNivel(oferta);
        return new ResponseEntity<>("Oferta actualizada correctamente.", HttpStatus.OK);
    }
}
