package com.grupodos.geoffers.replication.modelos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ClientCompanyOffer {
	@Id
	private String id;
	private String coClient;
	private String coCompany;
	private String coOffer;
	private String razonSocialCompany;
	private String nombreComercialCompany;
	private String sedeCompany;
	private Double latitudCompany;
	private Double longitudCompany;
	private String descripcionOffer;
	private String imagenOffer;
	private String productoOffer;
	private String subDescripcionOffer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCoClient() {
		return coClient;
	}

	public void setCoClient(String coClient) {
		this.coClient = coClient;
	}

	public String getCoCompany() {
		return coCompany;
	}

	public void setCoCompany(String coCompany) {
		this.coCompany = coCompany;
	}

	public String getCoOffer() {
		return coOffer;
	}

	public void setCoOffer(String coOffer) {
		this.coOffer = coOffer;
	}

	public String getRazonSocialCompany() {
		return razonSocialCompany;
	}

	public void setRazonSocialCompany(String razonSocialCompany) {
		this.razonSocialCompany = razonSocialCompany;
	}

	public String getNombreComercialCompany() {
		return nombreComercialCompany;
	}

	public void setNombreComercialCompany(String nombreComercialCompany) {
		this.nombreComercialCompany = nombreComercialCompany;
	}

	public String getSedeCompany() {
		return sedeCompany;
	}

	public void setSedeCompany(String sedeCompany) {
		this.sedeCompany = sedeCompany;
	}

	public Double getLatitudCompany() {
		return latitudCompany;
	}

	public void setLatitudCompany(Double latitudCompany) {
		this.latitudCompany = latitudCompany;
	}

	public Double getLongitudCompany() {
		return longitudCompany;
	}

	public void setLongitudCompany(Double longitudCompany) {
		this.longitudCompany = longitudCompany;
	}

	public String getDescripcionOffer() {
		return descripcionOffer;
	}

	public void setDescripcionOffer(String descripcionOffer) {
		this.descripcionOffer = descripcionOffer;
	}

	public String getImagenOffer() {
		return imagenOffer;
	}

	public void setImagenOffer(String imagenOffer) {
		this.imagenOffer = imagenOffer;
	}

	public String getProductoOffer() {
		return productoOffer;
	}

	public void setProductoOffer(String productoOffer) {
		this.productoOffer = productoOffer;
	}

	public String getSubDescripcionOffer() {
		return subDescripcionOffer;
	}

	public void setSubDescripcionOffer(String subDescripcionOffer) {
		this.subDescripcionOffer = subDescripcionOffer;
	}
}
