package com.grupodos.geoffers.replication;

import com.grupodos.geoffers.modelos.OfferClient;

public interface ReplicationClientCompanyOffer {
	void synchOfferClient(OfferClient offerClient);

	void syncAllDatabase();
}
