package com.grupodos.geoffers.replication.impl;

import java.util.List;

import com.grupodos.geoffers.modelos.CompanyModel;
import com.grupodos.geoffers.modelos.CompanyOffer;
import com.grupodos.geoffers.modelos.OfferClient;
import com.grupodos.geoffers.replication.ReplicationClientCompanyOffer;
import com.grupodos.geoffers.replication.modelos.ClientCompanyOffer;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompany;
import com.grupodos.geoffers.servicios.repositorio.RepositorioCompanyOffer;
import com.grupodos.geoffers.servicios.repositorio.RepositorioOfferClient;
import com.grupodos.geoffers.servicios.repositorio.RepositoryClientCompanyOffer;
import com.mongodb.bulk.BulkWriteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

@Component
public class ReplicationClientCompanyOfferImpl implements ReplicationClientCompanyOffer {

	private static final Logger log = LoggerFactory.getLogger(ReplicationClientCompanyOffer.class);

	private final MongoTemplate mongoTemplate;
	private final RepositorioCompany repositorioCompany;
	private final RepositorioCompanyOffer repositorioCompanyOffer;
	private final RepositoryClientCompanyOffer repositoryClientCompanyOffer;
	private final RepositorioOfferClient repositorioOfferClient;

	public ReplicationClientCompanyOfferImpl(MongoTemplate mongoTemplate, RepositorioCompany repositorioCompany, RepositorioCompanyOffer repositorioCompanyOffer, RepositoryClientCompanyOffer repositoryClientCompanyOffer, RepositorioOfferClient repositorioOfferClient) {
		this.mongoTemplate = mongoTemplate;
		this.repositorioCompany = repositorioCompany;
		this.repositorioCompanyOffer = repositorioCompanyOffer;
		this.repositoryClientCompanyOffer = repositoryClientCompanyOffer;
		this.repositorioOfferClient = repositorioOfferClient;
	}

	private ClientCompanyOffer buildModelClientCompanyOffer(OfferClient offerClient) {
		CompanyOffer offer = repositorioCompanyOffer.findById(offerClient.getOferta())//
				.orElseThrow(() -> new RuntimeException("No existe offerta"));

		CompanyModel company = repositorioCompany.findById(offer.getRucCompany())//
				.orElseThrow(() -> new RuntimeException("No existe offerta"));

		ClientCompanyOffer model = new ClientCompanyOffer();
		model.setCoClient(offerClient.getDniCliente());
		model.setCoCompany(company.getRuc());
		model.setCoOffer(offer.getId());
		model.setRazonSocialCompany(company.getRazonSocial());
		model.setNombreComercialCompany(company.getNombreComercial());
		model.setSedeCompany(company.getSede());
		model.setLongitudCompany(company.getLocation().getX());
		model.setLatitudCompany(company.getLocation().getY());
		model.setDescripcionOffer(offer.getDescripcion());
		model.setImagenOffer(offer.getImagen());
		model.setProductoOffer(offer.getProducto());
		model.setDescripcionOffer(offer.getDescripcion());
		return model;
	}

	@Override
	public void synchOfferClient(OfferClient offerClient) {
		ClientCompanyOffer model = this.buildModelClientCompanyOffer(offerClient);
		repositoryClientCompanyOffer.insert(model);
	}

	@Override
	public void syncAllDatabase() {
		BulkOperations bulkOperations = mongoTemplate
				.bulkOps(BulkOperations.BulkMode.UNORDERED, ClientCompanyOffer.class);
		List<OfferClient> offerClients = this.repositorioOfferClient.findAll();
		for (OfferClient i : offerClients) {
			try {
				ClientCompanyOffer m = this.buildModelClientCompanyOffer(i);
				bulkOperations.insert(m);
			}
			catch (Exception e) {
				log.error("Build model was failed: {} {}", i.getDniCliente(), i.getOferta(), e);
			}
		}
		this.repositoryClientCompanyOffer.deleteAll();
		BulkWriteResult rs = bulkOperations.execute();
		log.info("The synchronization process was completed with {} documents created", rs.getInsertedCount());
	}
}
