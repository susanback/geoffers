package com.grupodos.geoffers.config;

import com.grupodos.geoffers.sychronization.SychronizationReplicaManager;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class GeofferStartupApplicationEvent implements ApplicationListener<ApplicationReadyEvent> {
	private final SychronizationReplicaManager sychronizationReplicaManager;

	public GeofferStartupApplicationEvent(SychronizationReplicaManager sychronizationReplicaManager) {
		this.sychronizationReplicaManager = sychronizationReplicaManager;
	}

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		sychronizationReplicaManager.performReplicationDatabase();
	}
}
