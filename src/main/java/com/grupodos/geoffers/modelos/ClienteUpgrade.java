package com.grupodos.geoffers.modelos;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ClienteUpgrade extends Cliente {

    private Integer ofertasRegistradas;
    private Nivel siguienteNivel;
    private Integer ofertasSiguienteNivel;

    public Integer getOfertasRegistradas() {
        return ofertasRegistradas;
    }

    public void setOfertasRegistradas(Integer ofertasRegistradas) {
        this.ofertasRegistradas = ofertasRegistradas;
    }

    public Nivel getSiguienteNivel() {
        return siguienteNivel;
    }

    public void setSiguienteNivel(Nivel siguienteNivel) {
        this.siguienteNivel = siguienteNivel;
    }

    public Integer getOfertasSiguienteNivel() {
        return ofertasSiguienteNivel;
    }

    public void setOfertasSiguienteNivel(Integer ofertasSiguienteNivel) {
        this.ofertasSiguienteNivel = ofertasSiguienteNivel;
    }
}
