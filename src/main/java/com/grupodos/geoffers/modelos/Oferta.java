package com.grupodos.geoffers.modelos;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection="companyOffer")
public class Oferta extends CompanyOffer {

    private Date fechaInicio;
    private Date fechaFin;
    private Nivel nivel;
    private CompanyModel negocio;

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public CompanyModel getNegocio() {
        return negocio;
    }

    public void setNegocio(CompanyModel negocio) {
        this.negocio = negocio;
    }
}
