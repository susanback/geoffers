package com.grupodos.geoffers.modelos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection="client")
public class Cliente {

    @Id
    private String documento;
    private String nombres;
    private String apellidos;
    private Integer estrellas;
    private Date fechaUpgrade;
    private List<ClienteTarjeta> tarjetas;

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Integer getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(Integer estrellas) {
        this.estrellas = estrellas;
    }

    public Date getFechaUpgrade() {
        return fechaUpgrade;
    }

    public void setFechaUpgrade(Date fechaUpgrade) {
        this.fechaUpgrade = fechaUpgrade;
    }

    public List<ClienteTarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<ClienteTarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }
}
