package com.grupodos.geoffers.modelos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
public class Nivel {

    @Id
    private String id;
    private Integer estrellas;
    private Integer minOfertas;
    private List<Tarjeta> tarjetas;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(Integer estrellas) {
        this.estrellas = estrellas;
    }

    public Integer getMinOfertas() {
        return minOfertas;
    }

    public void setMinOfertas(Integer minOfertas) {
        this.minOfertas = minOfertas;
    }

    public List<Tarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }
}
