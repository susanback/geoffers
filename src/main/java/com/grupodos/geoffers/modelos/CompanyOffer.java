package com.grupodos.geoffers.modelos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CompanyOffer {

	@Id
	private String id;
	private String rucCompany;
	private String descripcion;
	private String imagen;
	private String producto;
	private String subdescripcion;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRucCompany() {
		return rucCompany;
	}

	public void setRucCompany(String rucCompany) {
		this.rucCompany = rucCompany;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getSubdescripcion() {
		return subdescripcion;
	}

	public void setSubdescripcion(String subdescripcion) {
		this.subdescripcion = subdescripcion;
	}

}
